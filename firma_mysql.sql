/*
SQLyog Community v13.1.2 (64 bit)
MySQL - 10.1.37-MariaDB : Database - firma
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`firma` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci */;

USE `firma`;

/*Table structure for table `firmy` */

DROP TABLE IF EXISTS `firmy`;

CREATE TABLE `firmy` (
  `id_firma` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `nip` mediumint(9) NOT NULL,
  `adres` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id_firma`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `firmy` */

/*Table structure for table `historia_pracownika` */

DROP TABLE IF EXISTS `historia_pracownika`;

CREATE TABLE `historia_pracownika` (
  `id_historia` int(11) NOT NULL AUTO_INCREMENT,
  `id_pracownik` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`id_historia`),
  KEY `historia_pracownika_ibfk_1` (`id_status`),
  KEY `historia_pracownika_ibfk_2` (`id_pracownik`),
  CONSTRAINT `historia_pracownika_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `status_pracownika` (`id_status`) ON UPDATE CASCADE,
  CONSTRAINT `historia_pracownika_ibfk_2` FOREIGN KEY (`id_pracownik`) REFERENCES `pracownicy` (`id_pracownik`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `historia_pracownika` */

/*Table structure for table `kontrakty` */

DROP TABLE IF EXISTS `kontrakty`;

CREATE TABLE `kontrakty` (
  `id_kontrakt` int(11) NOT NULL AUTO_INCREMENT,
  `typ` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `placa` int(11) NOT NULL,
  PRIMARY KEY (`id_kontrakt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `kontrakty` */

/*Table structure for table `pracownicy` */

DROP TABLE IF EXISTS `pracownicy`;

CREATE TABLE `pracownicy` (
  `id_pracownik` int(11) NOT NULL AUTO_INCREMENT,
  `imie` text COLLATE utf8_polish_ci NOT NULL,
  `nazwisko` text COLLATE utf8_polish_ci NOT NULL,
  `id_firma` int(11) NOT NULL,
  `id_kontrakt` int(11) NOT NULL,
  PRIMARY KEY (`id_pracownik`),
  KEY `pracownicy_ibfk_1` (`id_firma`),
  KEY `pracownicy_ibfk_2` (`id_kontrakt`),
  CONSTRAINT `pracownicy_ibfk_1` FOREIGN KEY (`id_firma`) REFERENCES `firmy` (`id_firma`) ON UPDATE CASCADE,
  CONSTRAINT `pracownicy_ibfk_2` FOREIGN KEY (`id_kontrakt`) REFERENCES `kontrakty` (`id_kontrakt`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `pracownicy` */

/*Table structure for table `status_pracownika` */

DROP TABLE IF EXISTS `status_pracownika`;

CREATE TABLE `status_pracownika` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `status_pracownika` */

/*Table structure for table `wyplaty` */

DROP TABLE IF EXISTS `wyplaty`;

CREATE TABLE `wyplaty` (
  `id_wyplaty` int(11) NOT NULL,
  `id_pracownik` int(11) NOT NULL,
  `nadgodziny` int(11) NOT NULL,
  `kwota` int(11) NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`id_wyplaty`),
  KEY `wyplaty_ibfk_1` (`id_pracownik`),
  CONSTRAINT `wyplaty_ibfk_1` FOREIGN KEY (`id_pracownik`) REFERENCES `pracownicy` (`id_pracownik`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

/*Data for the table `wyplaty` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
